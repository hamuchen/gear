( function ($) {
  Drupal.behaviors.gearToggleHeader = {
    attach: function (context, settings) {
      $('.gear-toggle-header').click(function () {
        $(this).next('.gear-toggle-content').fadeToggle(300);
      });
    }
  };
}(jQuery));
