( function ($) {
  Drupal.behaviors.gearVerticalHeader = {
    attach: function (context) {
      $('table', context).each(function () {
        var header_height = 0;
        $(this).find('th.vertical > *').each(function () {
          if ($(this).outerWidth() > header_height) {
            header_height = $(this).outerWidth();
          }
          $(this).width($(this).height());
          // hack for admin theme
          $(this).css('background-color', 'transparent');
        });
        $(this).find('th.vertical.change-height', context).height(header_height);
      });
    }
  };
}(jQuery));

