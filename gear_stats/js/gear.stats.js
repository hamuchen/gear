( function ($) {
  Drupal.behaviors.gearStatsGraph = {
    attach: function (context, settings) {
      // Line graph
      var lines = settings.gearLineGraph;
      if (typeof(lines) != 'undefined') {
        $.each(lines, function (id) {
          var options = this;
          options.dateFormat = function (x) {
            return gearFormatDate(new Date(x));
          };
          options.xLabelFormat = function (x) {
            return gearFormatDate(new Date(x));
          };
          options.yLabelFormat = function (x) {
            if (x != undefined) {
              return x.toLocaleString() + " " + options.unit;
            }
            return '-';
          };

          $("#canvas-wrapper-line-" + id).bind("inview", function () {
            var $this = $(this);
            $this.unbind("inview");
            setTimeout(function () {
              new Morris.Line(options);
            }, 100);
          });
        });
      }

      // Pie graph
      var pies = settings.gearPieGraph;
      if (typeof(pies) != 'undefined') {

        $.each(pies, function (id) {
          var options = this;
          options.formatter = function (y, data) {
            return parseFloat(y).toLocaleString() + " " + options.unit;
          };

          $("#canvas-wrapper-pie-" + id).bind("inview", function () {
            var $this = $(this);
            $this.unbind("inview");
            setTimeout(function () {
              new Morris.Donut(options);
            }, 300);
          });
        });
      }
    }
  };

  function gearFormatDate(d) {
    var dd = d.getDate();
    if (dd < 10) {
      dd = '0' + dd;
    }
    var mm = d.getMonth() + 1;
    if (mm < 10) {
      mm = '0' + mm;
    }
    var yy = d.getFullYear() % 100;
    if (yy < 10) {
      yy = '0' + yy;
    }
    return dd + '.' + mm + '.' + yy;
  }

}(jQuery));
