<?php

/**
 * @file
 * Hooks provided by gear action
 *
 */

/**
 * Action callback
 *
 * @param string $action the action name to process
 * @param array $parameters query parameters
 */
function hook_gear_action($action, $parameters) {
  switch ($action) {
    case 'actionname':
      // do some access/permission checks
      // do the action
      break;
    default:
      break;
  }
}
